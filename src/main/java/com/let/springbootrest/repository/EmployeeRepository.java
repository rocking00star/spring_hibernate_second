package com.let.springbootrest.repository;

import com.let.springbootrest.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository; 

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}